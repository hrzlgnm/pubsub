#include <iostream>

#include "bus.h"
#include "messagebus.h"
#include "petsubscriber.h"

int main()
{
    srand(time(0));
    auto twitch{std::make_shared<Bus>()};
    auto twitchtv{std::make_shared<MessageBus>(twitch)};
    auto petsub1{std::make_shared<PetSubscriber>(twitchtv)};
    auto petsub2{std::make_shared<PetSubscriber>(twitchtv)};
    auto petsub3{std::make_shared<PetSubscriber>(twitchtv)};
    auto petsub4{std::make_shared<PetSubscriber>(twitchtv)};
    auto petsub5{std::make_shared<PetSubscriber>(twitchtv)};
    auto petsub6{std::make_shared<PetSubscriber>(twitchtv)};
    auto petsub7{std::make_shared<PetSubscriber>(twitchtv)};
    auto petsub8{std::make_shared<PetSubscriber>(twitchtv)};
    auto petsub9{std::make_shared<PetSubscriber>(twitchtv)};


    const Cat c;
    const Dog d;
    for(int i = 0; i < 10; ++i) {
        twitchtv->pub(c);
        twitchtv->pub(d);
    }

    return 0;
}
