#ifndef PETSUBSCRIBER_H
#define PETSUBSCRIBER_H

#include "messagebus.h"
#include "messagesubscriber.h"
#include "pets.h"

class PetSubscriber
        : public MessageSubscriber<Cat>,
          public MessageSubscriber<Dog>
{
public:
    PetSubscriber(MessageBusPtr messagebus);

protected:
    void onMsg(const Cat& c) override;
    void onMsg(const Dog& c) override;

private:
    MessageBusPtr mamessagebus;
};
using PetSubscriberPtr = std::shared_ptr<PetSubscriber>;

#endif // PETSUBSCRIBER_H
