#ifndef MESSAGEBUS_H
#define MESSAGEBUS_H

#include "bus.h"
#include "messagesubscriber.h"

class MessageBus {
public:
    explicit MessageBus(BusPtr bus);

    template<typename Message>
    void sub(MessageSubscriber<Message> *subber, const std::string& topic) {
        mabus->sub(subber, topic);
    }

    template<typename Message>
    void unsub(MessageSubscriber<Message> *subber) {
        mabus->unsub(subber);
    }

    template<typename Message>
    void pub(const Message & m) {
        mabus->pub(Message::topic);
    }
private:
    BusPtr mabus;
};
using MessageBusPtr = std::shared_ptr<MessageBus>;

#endif // TWITCH_H
