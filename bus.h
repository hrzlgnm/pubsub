#ifndef BUS_H
#define BUS_H

#include <string>
#include <unordered_map>
#include <unordered_set>
#include <memory>

class Bus
{
public:
    struct Subscriber {
        virtual ~Subscriber() = default;
        virtual void onMsg() = 0;
    };

    void pub(const std::string& topic);
    void sub(Subscriber* sub, const std::string& topic);
    void unsub(Subscriber* sub);

private:
    void lazy_unsub();
    std::unordered_map<std::string, std::unordered_set<Subscriber*>> m_subs;
    std::unordered_set<Subscriber*> m_lazy_unsubs;
};

using BusPtr = std::shared_ptr<Bus>;
#endif // BUS_H
