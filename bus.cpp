#include "bus.h"
#include <iostream>

void Bus::pub(const std::string &topic)
{

    lazy_unsub();
    auto itr = m_subs.find(topic);
    if (itr != m_subs.end()) {
        for (const auto& s: (*itr).second) {
            s->onMsg();
        }
    } else {
        std::cout << "no sub for topic " << topic << " found\n";
    }
}

void Bus::sub(Subscriber *sub, const std::string &topic)
{
    std::cout << "@" << sub << " is subscribing on " << topic << "\n";
    m_subs[topic].insert(sub);
}

void Bus::unsub(Subscriber *sub)
{
    std::cout << "@" << sub << " unsubscribed\n";
    m_lazy_unsubs.insert(sub);
}

void Bus::lazy_unsub()
{
    std::unordered_set<std::string> emtpy_topics;
    for(auto &unsubber: m_lazy_unsubs) {
        for(auto &[topic, subs] : m_subs) {
            subs.erase(unsubber);
            if (subs.empty()) {
                emtpy_topics.insert(topic);
            }
        }
    }
    for(const auto &e : emtpy_topics) {
        std::cout << "topic " << e << " became empty, removing\n";
        m_subs.erase(e);
    }
}

