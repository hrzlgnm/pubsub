#ifndef MESSAGESUBSCRIBER_H
#define MESSAGESUBSCRIBER_H

#include "bus.h"
#include <iostream>

template<typename Message>
class MessageSubscriber : public Bus::Subscriber {
public:
    virtual void onMsg(const Message& msg) = 0;
    void onMsg() final
    {
       onMsg(Message{});
    }
};

#endif // MESSAGESUBSCRIBER_H
