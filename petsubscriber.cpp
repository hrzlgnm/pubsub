#include <iostream>
#include <cstdlib>
#include "petsubscriber.h"

inline constexpr auto catUnsubscribeProb = 2;
inline constexpr auto dogUnsubscribeProb = 3;

PetSubscriber::PetSubscriber(MessageBusPtr messagebus)
    : mamessagebus(std::move(messagebus))
{
    mamessagebus->sub<Cat>(this, Cat::topic);
    mamessagebus->sub<Dog>(this, Dog::topic);
}

void PetSubscriber::onMsg(const Cat &c)
{
    if (rand() % catUnsubscribeProb == 0) {
        mamessagebus->unsub<Cat>(this);
    }

}

void PetSubscriber::onMsg(const Dog &c)
{
    if (rand() % dogUnsubscribeProb == 0) {
        mamessagebus->unsub<Dog>(this);
    }
}
